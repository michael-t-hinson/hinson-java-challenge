import java.io.*;
import java.util.*;

public class StringManipulator {

    private static String cleanString(String str) {

        //Utilized Stringbuilder for ease of changes to string
        StringBuilder sb = new StringBuilder(str.toUpperCase());
        for(int i = 0; i < sb.length(); i++)
        {
            if(!Character.isLetter(sb.charAt(i)))
            {
                sb.deleteCharAt(i);
                i = i-1;//If a character is removed i+1 index will now be at i
            }
        }
        return sb.toString();
    }
    private static boolean hasUniqueChars(String word) {
        word = cleanString(word);
        Set<Character> lettersHash = new HashSet<>(word.length());

        //Utilized Hash Set for easy check of duplicates
        for(int i = 0; i < word.length(); i++)
        {
            if(lettersHash.contains(word.charAt(i)))
                return false;//Breaks loop to avoid unnecessary iterations
            else
                lettersHash.add(word.charAt(i));
        }
        return true;
    }
    private static double getWeight(String word) {
        word = cleanString(word);
        double asciiWeight = 0;
        for(int i = 0; i < word.length(); i++)
        {
            asciiWeight += (int)word.charAt(i);
        }
        return asciiWeight/word.length();
    }
    private static String[] sortStrings(String[] arr) {
        /*Function to sort array using insertion sort*/

        //Clean Strings in arr
        for (int i = 0; i < arr.length; i++)
        {
            arr[i] = cleanString(arr[i]);
        }
        double[] weightArr = new double[arr.length];
        //Creates array of calculations
        for(int i = 0; i < arr.length; i++)
        {
            weightArr[i] = getWeight(arr[i]);
        }
        int n = arr.length;
        for (int i=1; i<n; ++i)
        {
            double key = weightArr[i];
            String keyString = arr[i];
            int j = i-1;

            /* Move elements of arr[0..i-1], that are
               greater than key, to one position ahead
               of their current position */
            while (j>=0 && weightArr[j] > key)
            {
                weightArr[j+1] = weightArr[j];
                arr[j+1] = arr[j];
                j = j-1;
            }
            weightArr[j+1] = key;
            arr[j+1] = keyString;
            }
            return arr;
    }
    private static int countLines(String aFile) throws IOException {

        //Counted lines in file to allow for easy array creation and avoidance of lists
        LineNumberReader reader = null;
        try {
            reader = new LineNumberReader(new FileReader(aFile));
            while ((reader.readLine()) != null);//This line had been deleted on accident while attempting to publish to bitbucket
            return reader.getLineNumber();
        } catch (Exception ex) {
            return -1;
        } finally {
            if(reader != null)
                reader.close();
        }
    }
    private static String[] readCSVFile(String file) {
        try {
            FileReader fileReader = new FileReader(file);
            int lineCount = countLines(file);

            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String[] csvArray = new String[lineCount-1];
            String line = bufferedReader.readLine();//Removes header Line. Need some way to check for header

            int i = 0;
            while((line = bufferedReader.readLine()) != null){
                csvArray[i] = line;
                i++;
            }
            bufferedReader.close();
            return csvArray;
        }
        catch (FileNotFoundException ex)
        {
            System.out.println("Unable to open file " + file);
        }
        catch(IOException ex) {
            System.out.println("Error reading file " + file);
        }
        return null;
    }
    private static void writeToCSVFile(String[] arr) throws FileNotFoundException {
        PrintWriter pw = new PrintWriter(new File("src/atdd/challenge_sorted.csv"));
        StringBuilder sb = new StringBuilder();
        String seperator = ",";
        //Header Line
        sb.append("Words");
        sb.append(seperator);
        sb.append("Unique");
        sb.append(seperator);
        sb.append("Weight");
        sb.append("\n");
        pw.write(sb.toString());
        sb.setLength(0);//Clears Stringbuilder sb
        //Writes STRING, TRUE||FALSE, WEIGHT
        for(String string : arr)
        {
            sb.setLength(0);//Clears Stringbuilder sb
            sb.append(string);
            sb.append(seperator);
            sb.append(hasUniqueChars(string));
            sb.append(seperator);
            sb.append(getWeight(string));
            sb.append("\n");
            pw.write(sb.toString());
        }
        pw.close();
        System.out.println("Runtime");
    }
    public static void main(String [] args) {
        long startTime = System.currentTimeMillis();
        //Read file and sort
        String file = "src/atdd/challenge_tests.csv";
        String[] stringArray = readCSVFile(file);
        sortStrings(stringArray);
        //Writing to file
        try {
            writeToCSVFile(stringArray);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        //Calculate run time
        long endTime   = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        System.out.println(totalTime);
    }

}

